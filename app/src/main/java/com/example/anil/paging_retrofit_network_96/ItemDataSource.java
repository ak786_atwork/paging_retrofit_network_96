package com.example.anil.paging_retrofit_network_96;

import android.arch.paging.PageKeyedDataSource;
import android.support.annotation.NonNull;
import android.util.Log;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ItemDataSource extends PageKeyedDataSource<Integer, Item> {

    private static final String TAG  = "ItemDataSource";
    static int count = 0;

    public static final int FIRST_PAGE = 1;
    public static final int PAGE_SIZE = 5;
    public   static final String SITE_NAME = "stackoverflow";
    @Override
    public void loadInitial(@NonNull LoadInitialParams params, @NonNull final LoadInitialCallback callback) {

        RetrofitClient.getInstance()
                .getApi()
                .getAnswers(FIRST_PAGE, PAGE_SIZE, SITE_NAME)
                .enqueue(new Callback<StackApiResponse>() {
                    @Override
                    public void onResponse(Call<StackApiResponse> call, Response<StackApiResponse> response) {
                        count++;
                        showLog("loadInitial "+count);

                        if (response.body() != null)
                        {
                            callback.onResult(response.body().items, null,FIRST_PAGE + 1);
                        }
                    }

                    @Override
                    public void onFailure(Call<StackApiResponse> call, Throwable t) {

                    }
                });
    }

    @Override
    public void loadBefore(@NonNull final LoadParams params, @NonNull final LoadCallback callback) {

        RetrofitClient.getInstance()
                .getApi()
                .getAnswers((int)params.key, PAGE_SIZE, SITE_NAME)
                .enqueue(new Callback<StackApiResponse>() {
                    @Override
                    public void onResponse(Call<StackApiResponse> call, Response<StackApiResponse> response) {

                        count++;
                        showLog("loadBefore "+count);

                        if(response.body() != null){
                            Integer key = ((int)params.key > 1) ? (int)params.key - 1 : null;
                            callback.onResult(response.body().items, key);
                        }
                    }

                    @Override
                    public void onFailure(Call<StackApiResponse> call, Throwable t) {

                    }
                });

    }

    @Override
    public void loadAfter(@NonNull final LoadParams params, @NonNull final LoadCallback callback) {

        RetrofitClient.getInstance()
                .getApi()
                .getAnswers((int)params.key, PAGE_SIZE, SITE_NAME)
                .enqueue(new Callback<StackApiResponse>() {
                    @Override
                    public void onResponse(Call<StackApiResponse> call, Response<StackApiResponse> response) {

                        count++;
                        showLog("loadAfter "+count);

                        if(response.body() != null){
                            Integer key = ((int)params.key > 1) ? (int)params.key + 1 : null;
                            callback.onResult(response.body().items, key);
                        }
                    }

                    @Override
                    public void onFailure(Call<StackApiResponse> call, Throwable t) {

                    }
                });
    }

    public void showLog(String text)
    {
        Log.d(TAG, text);
    }
}
