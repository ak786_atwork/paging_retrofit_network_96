package com.example.anil.paging_retrofit_network_96;

import android.arch.paging.PagedListAdapter;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

public class ItemAdapter extends PagedListAdapter<Item, ItemAdapter.ItemViewHolder> {

    private Context context;

    public ItemAdapter(Context context) {
        super(diffCallback);
        this.context = context;
    }

    private static DiffUtil.ItemCallback<Item> diffCallback = new DiffUtil.ItemCallback<Item>() {
        @Override
        public boolean areItemsTheSame(@NonNull Item oldItem, @NonNull Item newItem) {
            return oldItem.answer_id == newItem.answer_id ? true : false;
        }

        @Override
        public boolean areContentsTheSame(@NonNull Item oldItem, @NonNull Item newItem) {
            return oldItem.equals(newItem);
        }
    };

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.recyclerview_item,viewGroup,false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder itemViewHolder, int i) {
        Item item = getItem(i);
        if (item != null)
        {
            Glide.with(context).load(item.owner.profile_image).into(itemViewHolder.profile_image);

            itemViewHolder.display_name.setText(item.owner.display_name);
        }
    }

    class ItemViewHolder extends RecyclerView.ViewHolder{

        public TextView display_name;
        public ImageView profile_image;
        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            display_name = itemView.findViewById(R.id.textview);
            profile_image = itemView.findViewById(R.id.imageview);
        }
    }

}
